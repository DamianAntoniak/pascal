program rolnik;
Uses CRT;
type
        osoba = record
                imie : string[24];
            nazwisko : string[24];
              wzrost : integer;
               klasa : string[3];
             srednia : real;
        end;
Var
        a : integer;
        uczen : array[1..2] of osoba;

Begin
        ClrScr;
        for a := 1 to 2 do
        begin
                Writeln('Uczen: ', a);
                Write('Podaj imie ucznia: '); Readln(uczen[a].imie);
                Write('Podaj nazwisko ucznia: '); Readln(uczen[a].nazwisko);
                Write('Podaj wzrost ucznia: '); Readln(uczen[a].wzrost);
                Write('Podaj klase ucznia: '); Readln(uczen[a].klasa);
                Write('Podaj srednia ucznia: '); Readln(uczen[a].srednia);
        end;

        Writeln('Wyswietlam');

        for a := 1 to 2 do
        begin
                With uczen[a] do
                begin

                        Writeln('UCZEN nr: ', a);
                        Writeln('Nazwisko: ', nazwisko);
                        Writeln('Imie: ', imie);
                        Writeln('Wzrost: ', wzrost);
                        Writeln('Klasa: ', klasa);
                        Writeln('Srednia: ', srednia:2:2);
                        Writeln;
                end;
        end;
readln;
end.