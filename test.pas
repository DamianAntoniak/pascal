Program zad3;
USES CRT;
function i(s : string) : boolean;
var F : text;
begin
	Assign(F, S);
	{$I-} Reset(F); {$I+}
	if IOResult = 0 then
	begin
		Close(F);
		i := true;
	end
	else
		i := false;
end;

function yo(x : boolean) : string;
begin
	if x = false then
		Writeln('Plik nie istnieje!')
	else
		Writeln('Plik istnieje');
end;

begin
		ClrScr;
		Writeln('C:\wax.txt'); 
		Writeln(yo(i('C:\wax.txt')));
		Writeln('C:\FPC\yo.txt'); 
		Writeln(yo(i('C:\FPC\yo.txt')));
		readln;
END.