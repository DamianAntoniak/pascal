program waxlolek;
Uses CRT;

const
	np = 'hehh.txt';

VAR
	f : Text;
	w : String;

BEGIN
	ClrScr;
	Assign(f, np);
	Rewrite(f);

	Writeln('Podaj text do zapisania');
	repeat
		readln(w);
		if w <>'' then
			writeln(f, w);
	until w ='';
	Close(f);
		
	Writeln('Zapisywanie do pliku zakonczono');
	readln;
END.