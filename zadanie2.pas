program cw2;
uses Crt;

const
Plik = 'F:\FPC\2.4.0\bin\i386-win32\TEXT.txt';

VAR
	f : Text;
	w, wax : String;
	I, counter : Byte;
	z : integer;
	
procedure file_default();
begin
	ClrScr;
	Assign(f, Plik);
	Reset(f);
	TextColor(9);
	Writeln('Zawartosc pliku:');
	TextColor(7);
	while not Eof(f) do
	begin
		readln(f, w);
		writeln(w);
	end;

	for I := 1 to length(w) do if w[i] = ' ' then
		counter := counter + 1;
			
	writeln;
	writeln('Ilosc slow: ', counter + 1);	
	readln;
end;

procedure file_own(s : string);
begin
	Assign(f, s);
	Reset(f);
	while not Eof(f) do
	begin
		readln(f, w);
		writeln(w);
	end;

	for I := 1 to length(w) do if w[i] = ' ' then
		counter := counter + 1;
			
	writeln;
	writeln('Ilosc slow: ', counter + 1);	
	readln;
end;

function file_Exists(s : string) : Boolean;
Var
	f : Text;
begin
	Assign(f, s);
	{$I-} Reset(f); {$I+}
	if IoResult = 0 then
	begin
		Close(f);
		file_Exists := true;
	end
	else file_Exists := false;
end;

BEGIN
	REPEAT
		ClrScr;
		TextBackGround(4);
		Writeln('Podaj odpowienia liczbe z menu:');
		Writeln('1: Plik domyslny; 2: Wlasny plik; 3: Exit;');
		TextBackGround(0);
	
		Readln(z);
		CASE z OF
			1 : file_default;
			2 : 
			begin
				ClrScr;
				Writeln('Podaj sciezke do pliku(sciezka zostanie sprawdzona)');
				Readln(wax);
				if(file_Exists(wax) = false) then Writeln('Taki plik nie istnieje!')
				else file_own(wax);
				readln;
			end; 
		end;
		
	UNTIL z = 3;
	exit;
	readln;
END.