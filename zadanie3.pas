program cw2;
uses Crt;

const
Plik = 'F:\FPC\2.4.0\bin\i386-win32\TEXT2.txt';

Var
	f 	 : Text;
	w 	 : String;
	i, z : integer;

procedure info;
begin
	i := 0;
	Assign(f, Plik);
	Reset(f);
	while not Eoln(f) do
	begin
		readln(f, w);
		Inc(i);
	end;
	Close(f);
end;

BEGIN
	ClrScr;
	
	info;
	TextColor(yellow);
	Writeln('(Plik ma: ', i,' linie)');
	TextColor(7);
	Writeln;
	Writeln('Podaj nr. wersu, od ktorego program ma wyswietlic zawartosc pliku.');
	Readln(z);
	
	i := 0;
	Assign(f, Plik);
	Reset(f);
	while not Eoln(f) do
	begin
		readln(f, w);
		Inc(i);
		if i >= z then writeln(w);
	end;
	readln;
END.