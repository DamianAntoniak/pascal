program sprzad1;
uses Crt;

Var
	p, d   : Text;
	l	   : string[70];
	n1, n2 : string[20];

Procedure Kopiuj(n1, n2 : string);
Begin
	ASSIGN(p, n1);
	Reset(p);
	ASSIGN(d, n2);
	Append(d);
	
	While NOT Eof(p) Do
	begin
		Readln(p, l);
		Writeln(d, l);
	end;
	Close(p);
	Close(d);
	TextColor(yellow);
	Writeln('Zawartosc pliku: ', n1, ' zostala dopisana do pliku: ', n2);
	TextColor(7);
end;

	
BEGIN
	ClrScr;
	Writeln;
	Write('Podaj nazwe pierwszego pliku: '); Readln(n1);
	Writeln;
	Write('Podaj nazwe drugiego pliku: '); Readln(n2);
	Kopiuj(n1, n2);
	readln;
END.	