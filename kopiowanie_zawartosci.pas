﻿Program kopiowanie;
USES CRT;

Var
	p, d   : Text;
	l	   : string[70];
	n1, n2 : string[20];

Procedure Kopiuj(n1, n2 : string);
Begin
	ASSIGN(p, n1);
	Reset(p);
	ASSIGN(d, n2);
	Append(d);
	
	While NOT Eof(p) Do
	begin
		Readln(p, l);
		Writeln(d, l);
	end;
	Close(p);
	Close(d);
	
	Writeln('Skopiowano zawartosc pliku: ', n1, ' do pliku: ', n2);
end;

	
BEGIN
	ClrScr;
	Writeln;
	Write('Podaj nazwe pliku wejsciowego: '); Readln(n1);
	Writeln;
	Write('Podaj nazwe pliku wyjsciowego: '); Readln(n2);
	Kopiuj(n1, n2);
	readln;
END.	