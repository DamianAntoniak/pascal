program hwdp;
uses Crt;

Var
pierwszy,drugi : Text;
linia : STRING[70];
naz_1,naz_2 : STRING[20];

PROCEDURE Kopiuj(naz_1,naz_2:STRING);
begin
ASSIGN(pierwszy,naz_1);
RESET(pierwszy);
ASSIGN(drugi,naz_2);
APPEND(drugi);
WHILE NOT Eof (pierwszy) DO
begin
Readln(pierwszy,linia);
Writeln(drugi,linia);
END;
Close(pierwszy);
Close(drugi);
Writeln('Skopiowano zawartosc pliku: ',naz_1,' do pliku: ',naz_2);
END;

Begin
Clrscr;
Writeln('Kopiowanie danych tekstowych');
Writeln('z pliku wejsciowego i dopisywanie ich');
Writeln('na koniec tekstu pliku wyjsciowego');
Writeln;
Write('Podaj nazwe pliku wejsciowego...;');
Readln(naz_1);
Write('Podaj nazwe pliku wyjscowego...:');
Readln(naz_2);
Kopiuj(naz_1,naz_2);
readln;
end.