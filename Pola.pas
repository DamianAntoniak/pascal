Unit Pola;

Interface
                function pro(x, y : integer) : integer;
                function row(x, z : integer) : integer;
                function tra(x, y, z : single) : single;

Implementation
                function pro(x, y : integer) : integer;
                begin
                        pro := x * y;
                end;

                function row(x, z : integer) : integer;
                begin
                        row := x * z;
                end;

                function tra(x, y, z : single) : single;
                begin
                        tra := 0.5 * (x + y) * z;
                end;

Begin
        Writeln('Hello!');
End.
