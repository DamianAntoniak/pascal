program recordy1;
Uses CRT, DOS;
Type 
	Tosoba = record
		Imie, Naz : String;
		Roku : word;
	end;
Var
	F : File of Tosoba;
	Osoba : Tosoba;
	rok, m, d, dt : word;
Begin
	GetDate(rok, m, d, dt);
	ClrScr;
	Assign(F, 'o.dta'); Rewrite(F);
	Repeat
		Write('Podaj imie: '); Readln(Osoba.Imie);
		if(Osoba.Imie <> '') then
		begin
			Write('Podaj nazwisko: '); 		Readln(Osoba.Naz);
			Write('Podaj rok urodzenia: '); 	Readln(Osoba.Roku);
			Write(F, Osoba); 				Writeln;
		end;
	Until (Osoba.Imie = '');
	Close(F);
	ClrScr;
	Reset(F);
	WHILE not Eof(F) do
	begin
		Read(F, Osoba);
		Writeln(Osoba.Imie, ' ', Osoba.Naz, ' ', rok-Osoba.Roku, ' lat');
	end;
	Close(F);
	Readln;
END.
	