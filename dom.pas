program  domek;
uses graph,strings;
var karta,tryb,i:integer;
napis,napis2:string;
pattern:array [1..8] of byte;

begin
for i:=1 to 8 do pattern[i]:=0;

detectgraph(karta,tryb);
initgraph(karta,tryb,'c:\');

setfillpattern( pattern,0);

setbkcolor(100); //niebo
bar(0,0,getmaxx,600);

setbkcolor(green); //trawa
bar(0,601,getmaxx,getmaxy);

//chmura 1
setcolor(blue);
setbkcolor(lightblue);

for i:=1 to 3 do begin circle(400+i*50,150,30);
                       floodfill(400+50*i,150,blue);end;
//chmura 2
for i:=1 to 3 do begin circle(800+i*50,100,30);
                       floodfill(800+50*i,100,blue);end;

setbkcolor(brown);
bar(200,400,600,600);     //dom
bar(800,300,820,600);     //drzewo

setbkcolor(darkgray);
bar(300,500,350,600); //drzwi

setbkcolor(blue);
bar(400,450,500,550);  //okno
setcolor(white);
line(400,500,500,500);
line(450,450,450,550);

//korona drzewa
setcolor(lightgreen);
circle(810,300,100);
setbkcolor(green);
floodfill(810,300,lightgreen);

setbkcolor(red);     //komin
bar(300,220,340,300);

//dach
setcolor(white);
line(200,400,600,400);
line(200,400,400,200);
line(400,200,600,400);
setbkcolor(darkgray);
floodfill(400,210,white);

//slonce
setcolor(lightcyan);
circle(200,100,50);
setbkcolor(yellow);
floodfill(200,100,lightcyan);

readln;
closegraph;
end.
