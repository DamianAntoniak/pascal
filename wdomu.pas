program wdomu;
Uses CRT, DOS;

Const
	days : array[0..6] of string = ('Niedziela', 'Poniedzialek', 'Wtorek', 'Sroda', 'Czwartek', 'Piatek', 'Sobota'); 
  
type
        osoba = record
                imie : string[24];
            nazwisko : string[24];
              wzrost : integer;
               klasa : string[3];
             srednia : real;
        end;

Var
        a : integer;
        uczen : array[1..6] of osoba;
        b : string[24];
        c : integer;
		z, x : integer;
		year, month, day, dd : word;
		plik : text;

		
procedure add;
begin
            TextColor(7);
            Write('----- ');
            TextColor(14); Write('UCZEN ');
            TextColor(red); Write(x);
            TextColor(7); Writeln(' -------');
            Write('Podaj imie ucznia: '); Readln(uczen[x].imie);
            Write('Podaj nazwisko ucznia: '); Readln(uczen[x].nazwisko);
            Write('Podaj wzrost ucznia: '); Readln(uczen[x].wzrost);
            Write('Podaj klase ucznia: '); Readln(uczen[x].klasa);
            Write('Podaj srednia ucznia: '); Readln(uczen[x].srednia);
			x := x + 1;
            Writeln;
			readln;
end;

procedure show;
begin
		TextColor(15);
        Writeln('Wyswietlam zawartosc:');
        TextColor(7);
        for a := 1 to x-1 do
        begin
                With uczen[a] do
                begin
                        TextColor(yellow);
                        Writeln('UCZEN nr: ', a);
                        TextColor(7);
                        Writeln('Nazwisko: ', nazwisko);
                        Writeln('Imie: ', imie);
                        Writeln('Wzrost: ', wzrost);
                        Writeln('Klasa: ', klasa);
                        Writeln('Srednia: ', srednia:2:2);
                        Writeln;
						readln;
                end;
        end;
end;

procedure search;
begin
		Writeln('Wyszukiwanie danych');
        Writeln;
        Writeln('Podaj nazwisko ucznia: '); Readln(b);
        Writeln;
        c := 1;
        for a := 1 to x do
        begin
                if(uczen[a].nazwisko = b) then
                begin
                        With uczen[a] do
                        begin
                                TextColor(yellow);
                                Writeln('UCZEN nr: ', a);
                                TextColor(7);
                                Writeln('Nazwisko: ', nazwisko);
                                Writeln('Imie: ', imie);
                                Writeln('Wzrost: ', wzrost);
                                Writeln('Klasa: ', klasa);
                                Writeln('Srednia: ', srednia:2:2);
                                Writeln;
                        end;
                end
                else c := c+1;

                if(c = 3) then
                        Writeln('Brak recordow o tym nazwisku');
			readln;			
        end;

end;

Begin
		x := 1;
		REPEAT
        ClrScr;
		TextBackGround(4);
		Writeln('1: Dodaj; 2: Wyszukaj; 3: Pokaz; 4: Data(zapisuje); 5: Exit;');
		TextBackGround(0);
		readln(z);
		
		CASE z OF
			1 : add;
			2 : search;
			3 : show;
			4 : 
				begin
					assign(plik, 'C:\Users\Student\Desktop\plik.txt');
					rewrite(plik);
					GetDate(year, month, day, dd);
					Writeln('Dzisiaj jest ', days[dd], '(', day, '.',  month, '.', year, ')');
					Writeln(plik, 'Dzisiaj jest ', days[dd], '(', day, '.',  month, '.', year, ')');
					close(plik);
					readln;
				end;
		end;
		
        UNTIL z = 5;
		
		Writeln('Zamykanie bazy trwa..');
		delay(3000);
		exit;
		
    readln;
End.
