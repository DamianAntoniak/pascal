program liczbypierwsze;
USES CRT;

Function pier(N : Longint) : Boolean;
var
	p : Boolean;
	i : Longint;
begin
	if(N = 2) then
		pier := True
	else
	begin
		P := True;
		for i := 2 to N - 1 do
			if P then
				if(N mod i = 0) then
					P := False;
		pier := p;
	end;
end;

VAR
	F : Text;
	G : File of Longint;
	I : Longint;

Begin
	ClrScr;
	Assign(F, 'pierw.txt'); Rewrite(F);
	Assign(G, 'pierw.dta'); Rewrite(G);
	I := 2;
	repeat
	    GotoXY(1, 1);
		Write(I);
		if Pier(I) then
		begin
			Writeln(F, I);
			Write(G, I);
		end;
		Inc(I);
	Until KeyPressed;
	Close(F); Close(G);
	Readln;
End.
