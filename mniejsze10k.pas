program mniejszerowne10k;
Uses CRT;
Var
	F : File of Longint;
	I, liczba, liczbapier, biez : Longint;

Begin
	ClrScr;
	Assign(F, 'pierw.dta'); Reset(F);
	Liczbapier := 0; bierz := 1;
	
	WHILE not Eof(F) do
	begin
		Read(F, liczba);
		for I := biez + 1 to liczba do
		begin
			if I = liczba then
				Inc(liczbapier);
			if I mod 10000 = 0 then
				writeln(I:10, liczbapier:10);
		end;
		biez := liczba;
	end;
	Close(F);
	Readln;
END.	