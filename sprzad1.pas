program sprzad1;
uses Crt;

type
	Tuczen = record
		Imie, Naz : String;
		Sred : Real;
	end;

VAR
	F : File of Tuczen;
	Uczen : Tuczen;
	z : integer;
	
BEGIN
	z := 0;
	Assign(F, 'F:\FPC\2.4.0\bin\i386-win32\recordy.DTA.'); Rewrite(F);
	REPEAT
		ClrScr;
		Writeln('Podaj Imie ucznia: '); Readln(Uczen.Imie);
		Writeln('Podaj Nazwisko ucznia: '); Readln(Uczen.Naz);
		Writeln('Podaj Srednia ucznia: '); Readln(Uczen.Sred);
		Write(F, Uczen); Writeln;
		Inc(z);
	Until z = 3;
	Close(F);

	ClrScr;
	Reset(F);
	z := 0;
	WHILE not Eof(F) do
	begin
		Read(F, Uczen);
		Inc(z);
		TextBackGround(4);
		Writeln('Uczen nr: ', z);
		TextBackGround(0);
		Writeln(Uczen.Imie, ' ', Uczen.Naz, ' ', Uczen.Sred:2:2, ' ');
		Writeln;
	end;
	Close(F);
	Readln;
END.