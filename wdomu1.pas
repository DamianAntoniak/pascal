program wdomu;
Uses CRT;
type
        osoba = record
                imie : string[24];
            nazwisko : string[24];
              wzrost : integer;
               klasa : string[3];
             srednia : real;
        end;
Var
        a : integer;
        uczen : array[1..2] of osoba;
        b : string[24];
        c : integer;

Begin
        ClrScr;
        for a := 1 to 2 do
        begin
                TextColor(7);
                Write('----- ');
                TextColor(14); Write('UCZEN ');
                TextColor(red); Write(a);
                TextColor(7); Writeln(' -------');
                Write('Podaj imie ucznia: '); Readln(uczen[a].imie);
                Write('Podaj nazwisko ucznia: '); Readln(uczen[a].nazwisko);
                Write('Podaj wzrost ucznia: '); Readln(uczen[a].wzrost);
                Write('Podaj klase ucznia: '); Readln(uczen[a].klasa);
                Write('Podaj srednia ucznia: '); Readln(uczen[a].srednia);
                Writeln;
        end;

        Writeln;
        TextColor(15);
        Writeln('Wyswietlam zawartosc:');
        TextColor(7);
        for a := 1 to 2 do
        begin
                With uczen[a] do
                begin
                        TextColor(yellow);
                        Writeln('UCZEN nr: ', a);
                        TextColor(7);
                        Writeln('Nazwisko: ', nazwisko);
                        Writeln('Imie: ', imie);
                        Writeln('Wzrost: ', wzrost);
                        Writeln('Klasa: ', klasa);
                        Writeln('Srednia: ', srednia:2:2);
                        Writeln;
                end;
        end;

        Writeln('Wyszukiwanie danych');
        Writeln;
        Writeln('Podaj nazwisko ucznia: '); Readln(b);
        Writeln;
        c := 1;
        for a := 1 to 2 do
        begin
                if(uczen[a].nazwisko = b) then
                begin
                        With uczen[a] do
                        begin
                                TextColor(yellow);
                                Writeln('UCZEN nr: ', a);
                                TextColor(7);
                                Writeln('Nazwisko: ', nazwisko);
                                Writeln('Imie: ', imie);
                                Writeln('Wzrost: ', wzrost);
                                Writeln('Klasa: ', klasa);
                                Writeln('Srednia: ', srednia:2:2);
                                Writeln;
                        end;
                end
                else c := c+1;

                if(c = 3) then
                        Writeln('Brak recordow o tym nazwisku');

        end;

    readln;
End.
